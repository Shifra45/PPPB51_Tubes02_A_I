package com.example.myapplication;

public class Url {
    public static final String URL_LOGIN = "https://devel.loconode.com/pppb/v1";

    public static final String E_INV_CRED = null;
    public static final String E_INV_USERNAME = "username";
    public static final String E_INV_PASSWORD = "password";

    public static final String OK = "success";
}
